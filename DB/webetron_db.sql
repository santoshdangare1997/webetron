-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2021 at 06:30 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webetron_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE `parent` (
  `parent_id` int(11) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `parent_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `parent_crt_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `structure`
--

CREATE TABLE `structure` (
  `struct_id` int(11) NOT NULL,
  `struct_name` varchar(255) NOT NULL,
  `struct_parent` varchar(255) NOT NULL,
  `struct_code` varchar(255) NOT NULL,
  `struct_desc` longtext NOT NULL,
  `struct_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `struct_crt_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `struct_mfd_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `structure`
--

INSERT INTO `structure` (`struct_id`, `struct_name`, `struct_parent`, `struct_code`, `struct_desc`, `struct_status`, `struct_crt_date`, `struct_mfd_date`) VALUES
(1, 'sd', '3', '1234', '<p>hello <b>world</b></p>', 'Active', '2021-04-10 11:35:11', '2021-04-10 11:35:11'),
(2, 'ddd', '4', '879', '<p>yuyutvtrtr t</p>', 'Active', '2021-04-10 11:43:25', '2021-04-10 11:43:25'),
(3, 'sss', '', '2323', '<p>eeeee eeee</p>', 'Active', '2021-04-10 12:47:58', '2021-04-10 12:47:58'),
(4, 'www', '', 'www', '<p><br></p>', 'Active', '2021-04-10 13:00:09', '2021-04-10 13:00:09'),
(5, '22', '5', 'we', '<p>www</p>', 'Active', '2021-04-10 13:01:38', '2021-04-10 13:01:38'),
(6, 'qqq', '5', '234', '<p>qswdfg</p>', 'Active', '2021-04-10 13:04:50', '2021-04-10 13:04:50'),
(7, 'qweddx', '5', '87654', '<p>dsfg r3rerere</p>', 'Active', '2021-04-10 13:06:14', '2021-04-10 13:06:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parent`
--
ALTER TABLE `parent`
  ADD PRIMARY KEY (`parent_id`);

--
-- Indexes for table `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`struct_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parent`
--
ALTER TABLE `parent`
  MODIFY `parent_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `structure`
--
ALTER TABLE `structure`
  MODIFY `struct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
