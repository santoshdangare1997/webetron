<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Structure extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Structure_model');
        $this->load->library('form_validation');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function submitStructure()
	{
        // $this->form_validation->set_rules('stru-name', 'stru-name', 'trim|required|min_length[2]|max_length[50]');
        // $this->form_validation->set_rules('stru-code', 'stru-code', 'trim|required|min_length[2]|max_length[15]');
        // $this->form_validation->set_rules('desc', 'desc', 'required');

        // if ($this->form_validation->run() == FALSE)
        // {
        //     return validation_errors(); 
        //          //add flash data 
        //  $this->session->set_flashdata('name-error',validation_errors());
        // }
        // else
        // {    
            $struc_data = array(   //into table field
                
                'struct_parent' => trim($this->input->post('stru-parent')),
                'struct_name'  => trim($this->input->post('stru-name')), 
                'struct_code' => trim($this->input->post('stru-code')),
                'struct_desc' => trim($this->input->post('desc')),
            );

            $result = $this->Structure_model->insert_data('structure',$struc_data);
            if($result==1){
                // $data['success']='successful';
                echo json_encode(1);
            }else{
                echo json_encode(0);
        
            }
        // }
	}

   public function fetchStruct(){

    $where_data=array('struct_status'=>'Active');
    $data['fetch_struct'] = $this->Structure_model->get_data('structure',$where_data);

    $this->load->view('struct_ajax_view',$data);

   } 
}
