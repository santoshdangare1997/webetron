<?php 

class Structure_model extends CI_Model {


    // get data function 
    public function get_data($table_name, $where_data) {

        $query = $this->db->get_where($table_name, $where_data);
        return ($query->num_rows() > 0) ? $query->result_array() : 'No record found';
    }

    // insert data function 
    public function insert_data($table_name, $insert_data) {

        return $this->db->insert($table_name, $insert_data);
    }

    // update data function 
    public function update_data($table_name, $update_data, $where_data) {
        return $this->db->update($table_name, $update_data, $where_data);
    }
  




}