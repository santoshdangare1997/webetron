
<div class="preview-structure h-100">
    <div class="card">
        <div class="card-header">
            <h6 class="mb-0">Structure</h6>
        </div>
        <div class="card-body">
            <div class="prev-structure-scroll air__customScroll w-100">

        <!-- dynamic data start -->
        <?php foreach($fetch_struct as $fetch_struct_ajax){ ?>
        <div class="dd" id="nestable1">
                <ol class="dd-list dd-level-1">
                    <li class="dd-item" data-id="<?php echo $fetch_struct_ajax['struct_parent']?>">
                    <div class="dd-outer">
                        <div class="dd-handle"><i class="fas fa-arrows-alt"></i></div>
                        <div class="dd-structure-item" data-placement="top" data-toggle="popover" data-html="true" data-content=''>
                            <div class="dd-structure-img">
                                <figure>
                                    <img src="<?php echo base_url(); ?>assets/images/user.jpg" alt="">
                                </figure>
                            </div>
                            <div class="dd-structure-content">
                                <!-- <div class="dd-structure-name">User name1</div> -->
                                <div class="dd-structure-name">User name<?php echo $fetch_struct_ajax['struct_parent']; ?></div>
                                <div class="dropdown">
                                    <button style="max-height:40px" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#"><i class="fas fa-plus"></i> Add</a>
                                    <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Edit</a>
                                    <a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a>
                                    </div>
                                </div>
                                <div class="dd-structure-code">#<?php echo $fetch_struct_ajax['struct_code'] ?></div>
                                <div class="dd-structure-description"><?php echo $fetch_struct_ajax['struct_desc'] ?></div>
                            </div>
                        </div>
                    </div>
                    </li>
                </ol>
        </div>  
        <?php } ?>   
        <!-- dynamic data end -->

    </div>
        </div>
        </div>
</div>
